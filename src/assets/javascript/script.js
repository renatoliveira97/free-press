const ativarBotao = (valor) => {
    switch(valor) {
        case 1:
            document.getElementById('link_2').classList.remove('active');
            document.getElementById('link_3').classList.remove('active');
            document.getElementById('link_4').classList.remove('active');
            document.getElementById('link_5').classList.remove('active');
            document.getElementById('link_1').classList.add('active');
            break;
        case 2:
            document.getElementById('link_1').classList.remove('active');
            document.getElementById('link_3').classList.remove('active');
            document.getElementById('link_4').classList.remove('active');
            document.getElementById('link_5').classList.remove('active');
            document.getElementById('link_2').classList.add('active');
            break;
        case 3:
            document.getElementById('link_1').classList.remove('active');
            document.getElementById('link_2').classList.remove('active');
            document.getElementById('link_4').classList.remove('active');
            document.getElementById('link_5').classList.remove('active');
            document.getElementById('link_3').classList.add('active');
            break;
        case 4:
            document.getElementById('link_1').classList.remove('active');
            document.getElementById('link_2').classList.remove('active');
            document.getElementById('link_3').classList.remove('active');
            document.getElementById('link_5').classList.remove('active');
            document.getElementById('link_4').classList.add('active');
            break;
        case 5:
            document.getElementById('link_1').classList.remove('active');
            document.getElementById('link_2').classList.remove('active');
            document.getElementById('link_3').classList.remove('active');
            document.getElementById('link_4').classList.remove('active');
            document.getElementById('link_5').classList.add('active');
            break;
    }
}

$('.carousel').carousel({
    pause: "false"
});